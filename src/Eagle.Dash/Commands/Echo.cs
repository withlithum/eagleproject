﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash.Commands
{
    public class Echo : ICommand<string>
    {
        public string ID => "echo";

        public void Call(string[] args)
        {
            if(args.Length == 0)
            {
                Console.WriteLine("echo: invaild parameters");
                return;
            }
            string full = "";
            foreach(string text in args)
            {
                if(full == "")
                {
                    full = text;
                }
                else
                {
                    full = full + " " + text;
                }
            }
            Console.WriteLine(full);
        }

        public string CallWithResult(string[] args)
        {
            throw new NotImplementedException();
        }

        public void InteropCommand(string cmd)
        {
            throw new NotImplementedException();
        }
    }
}
