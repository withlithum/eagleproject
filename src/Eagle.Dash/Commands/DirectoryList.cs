﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash.Commands
{
    public class DirectoryList : ICommand<string>
    {
        public string ID => "ls";

        public void Call(string[] args)
        {
            string[] fileNames = Directory.GetFiles(Environment.CurrentDirectory);
            string files = "";
            foreach(string fileName in fileNames)
            {
                if(files == "")
                {
                    files = Path.GetFileName(fileName);
                }
                else
                {
                    files = files + "       " + Path.GetFileName(fileName);
                }
            }
            string[] directories = Directory.GetDirectories(Environment.CurrentDirectory);
            string directory = "";
            foreach (string folder in directories)
            {
                if (files == "")
                {
                    directory = Path.GetFileName(folder);
                }
                else
                {
                    directory = directory + "       " + Path.GetFileName(folder);
                }
            }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(files);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(directory);
            Console.ResetColor();
            
        }

        public string CallWithResult(string[] args)
        {
            throw new NotImplementedException();
        }

        public void InteropCommand(string cmd)
        {
            throw new NotImplementedException();
        }
    }
}
