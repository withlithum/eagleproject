﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash.Commands
{
    class ExternalProcess : ICommand<string>
    {
        public string ID {
            get
            {
                return "ext_proc";
            }
        }

        public void Call(string[] args)
        {
            if(args.Length == 0)
            {
                return;
            }

            if(args.Length == 1)
            {

            }
        }

        public string CallWithResult(string[] args)
        {
            throw new InvalidOperationException("This command returns no result.");
        }

        public void InteropCommand(string cmd)
        {
            throw new NotImplementedException();
        }

        public static void StartExternalProcess(string fileName, string[] arguments)
        {
			// Start new thread
			string arg = "";
			string fullName = fileName;
			if (arguments.Length == 0)
			{
				arg = "";
			}
			if(!fileName.Contains(Path.DirectorySeparatorChar))
			{
				fileName = Path.Combine(Environment.CurrentDirectory, fileName);
			}
			else
			{
				foreach (string argument in arguments)
				{
					if (argument == fileName)
					{
						continue;
					}
					if (arg == "")
					{
						arg = argument;
					}
					else
					{
						arg = arg + " " + argument;
					}
				}
			}
			var startinfo = new ProcessStartInfo(fileName)
			{
				CreateNoWindow = true,
				UseShellExecute = false,
				Arguments = arg,
				RedirectStandardOutput = true,
				RedirectStandardError = true,
			};
			try
			{
				var process = new Process { StartInfo = startinfo };
				process.Start();

				var reader = process.StandardOutput;
				while (!reader.EndOfStream)
				{
					// the point is that the stream does not end until the process has 
					// finished all of its output.
					Console.WriteLine(reader.ReadLine());
				}

				process.WaitForExit();
			}
			catch (Win32Exception wex)
			{
				switch (wex.HResult)
				{
					case -2147467259:
						Console.WriteLine(fullName + ": command not found");
						break;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(fullName + ": failed to launch: " + ex.Message + Environment.NewLine + ex.ToString());
			}
		}
    }
}
