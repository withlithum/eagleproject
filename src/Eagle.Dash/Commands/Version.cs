﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash.Commands
{
    public class Version : ICommand<string>
    {
        public string ID => "ver";

        public void Call(string[] args)
        {
            Console.WriteLine("Eagle Dash, version " + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            string SystemBit = Environment.Is64BitOperatingSystem ? "x64" : "x86";
            string ProcessBit = Environment.Is64BitProcess ? "x64" : "x86";
            Console.WriteLine("Running on .NET version " + Environment.Version + " with OS " + Environment.OSVersion + " ("
                + SystemBit + " system, " + ProcessBit + " process)");
            Console.WriteLine("Username: " + Environment.UserName);
            Console.WriteLine("Machine Name: " + Environment.MachineName);
        }

        public string CallWithResult(string[] args)
        {
            throw new NotImplementedException();
        }

        public void InteropCommand(string cmd)
        {
            throw new NotImplementedException();
        }
    }
}
