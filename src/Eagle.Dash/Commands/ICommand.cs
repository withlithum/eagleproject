﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash.Commands
{
    interface ICommand<T>
    {
        void InteropCommand(string cmd);
        string ID { get; }
        void Call(string[] args);
        T CallWithResult(string[] args);
    }
}
