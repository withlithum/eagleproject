﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash.Commands
{
    public class ChangeDirectory : ICommand<string>
    {
        public string ID => "cd";

        public void Call(string[] args)
        {
            if(args.Length != 1)
            {
                Console.WriteLine("cd: Invaild Parameters");
                return;
            }

            if(!Directory.Exists(args[0]))
            {
                Console.WriteLine("cd: Directory Not Exists");
                return;
            }

            
            Environment.CurrentDirectory = args[0];
            Common.SharedVars.CurrentFolder = Environment.CurrentDirectory;
        }

        public string CallWithResult(string[] args)
        {
            throw new NotImplementedException();
        }

        public void InteropCommand(string cmd)
        {
            throw new NotImplementedException();
        }
    }
}
