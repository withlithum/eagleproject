﻿using Eagle.Dash.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eagle.Dash
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Eagle Dash, version" + Assembly.GetExecutingAssembly().GetName().Version.ToString());
            SharedVars.CurrentFolder = Environment.CurrentDirectory;
            Console.WriteLine("Copyright(C) 2019 RelaperCrystal" + Environment.NewLine);
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(SharedVars.CurrentFolder);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(" | [dash]>$ ");
                Console.ResetColor();
                ProcessCmd(Console.ReadLine().Split(' '));
            }
        }

        static void ProcessCmd(string[] commands)
        {
            if (commands.Length == 0)
            {
                return;
            }
            List<string> args = new List<string>();
            string cmdName = commands[0];
            if (commands.Length == 1)
            {
                ParseCmd(cmdName, new string[0]);
            }
            else
            {
                foreach (string cmd in commands)
                {
                    if (cmd == cmdName)
                    {
                        continue;
                    }
                    else
                    {
                        args.Add(cmd);
                    }
                }
                ParseCmd(cmdName, args.ToArray());
            }

            
        }

        public static void ParseCmd(string name, string[] arguments)
        {
            switch (name)
            {
                default:
                    Commands.ExternalProcess.StartExternalProcess(name, arguments);
                    break;
                case "echo":
                    Commands.Echo echo = new Commands.Echo();
                    echo.Call(arguments);
                    break;
                case "ver":
                    new Commands.Version().Call(arguments);
                    break;
                case "cd":
                    new Commands.ChangeDirectory().Call(arguments);
                    break;
                case "ls":
                    new Commands.DirectoryList().Call(arguments);
                    break;
            }
            Console.WriteLine();
        }
    } }
