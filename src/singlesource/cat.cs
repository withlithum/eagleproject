using System;
using System.IO;

class EagleCat
{
	static void Main(string[] args)
	{
		if(args.Length != 1)
		{
			Console.WriteLine("cat: invaild arguments");
			return;
		}
		
		if(!File.Exists(args[0]))
		{
			Console.WriteLine("cat: file not found");
			return;
		}
		try
		{
			string text = File.ReadAllText(args[0]);
			
			if(text == "")
			{
				Console.WriteLine("cat: empty file");
				return;
			}
			
			Console.WriteLine(text);
		}
		catch(Exception ex)
		{
			Console.WriteLine("cat: exception thrown: " + Environment.NewLine + ex.ToString());
			return;
		}
		
		
	}
}