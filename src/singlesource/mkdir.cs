using System;
using System.IO;

class EagleMakeDir
{
	static void Main(string[] args)
	{
		if(args.Length != 1)
		{
			Console.WriteLine("mkdir: invaild arguments");
			return;
		}
		
		try
		{
			Directory.CreateDirectory(args[0]);
		}
		catch(Exception ex)
		{
			Console.WriteLine("mkdir: exception thrown: " + Environment.NewLine + ex.ToString());
			return;
		}
	}
}