/*  Eagle Dash
    Copyright (C) 2019  RelaperCrystal

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>. */

using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;

public class Bash
{
	public static string currentFolder = Environment.CurrentDirectory;
	public static void StartNewProg(string fileName, string[] args)
	{
		
		// Start new thread
		string arg = "";
		if(args.Length == 0)
		{
			arg = "";
		}
		else
		{
			foreach(string argument in args)
			{
				if(argument == fileName)
				{
					continue;
				}
				if(arg == "")
				{
					arg = argument;
				}
				else
				{
					arg = arg + " " + argument;
				}
			}
		}
		var startinfo = new ProcessStartInfo(fileName)
		{
			CreateNoWindow = true,
			UseShellExecute = false,
			Arguments = arg,
			RedirectStandardOutput = true,
			RedirectStandardError = true,
		};
		try
		{
		var process = new Process { StartInfo = startinfo };
		process.Start();

		var reader = process.StandardOutput;
		while (!reader.EndOfStream)
		{
			// the point is that the stream does not end until the process has 
			// finished all of its output.
			Console.WriteLine(reader.ReadLine());
		}

		process.WaitForExit();
		}
		catch(Win32Exception wex)
		{
			switch(wex.HResult)
			{
				case -2147467259:
					Console.WriteLine(fileName + ": command not found");
					break;
			}
		}
		catch(Exception ex)
		{
			Console.WriteLine(fileName + ": failed to launch: " + ex.Message + Environment.NewLine + ex.ToString());
		}
	}
	public static void Main(string[] args)
	{
		Console.WriteLine("Eagle Dash");
		Console.WriteLine("Copyright(C) RelaperCrystal 2019" + Environment.NewLine);
		while(true)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.Write(currentFolder);
			Console.ForegroundColor = ConsoleColor.White;
			Console.Write(" |[dash]$ ");
			Console.ResetColor();
			string lne = Console.ReadLine();
			ProgramCmd(lne);
		}
	}
	public static void ProgramCmd(string lines)
	{
		if(lines == "")
		{
			Console.WriteLine(Environment.NewLine);
			return;
		}
		string[] lnes = lines.Split(' ');
		string appName;
		if(lnes.Length == 1)
		{
			appName = lnes[0];
			string[] empty = new string[0];
		}
		else
		{
			List<string> newArg = new List<string>();
				foreach(string argument in lnes)
				{
					if(argument == lnes[0])
					{
						continue;
					}
					newArg.Add(argument);
				}
				appName = lnes[0];
				lnes = newArg.ToArray();
		}
		GetCmd(appName, lnes);
	}
	public static void GetCmd(string name, string[] args)
	{
		switch(name)
		{
			case "cd":
				if(args.Length != 1)
				{					
					Console.WriteLine("cd: Invaild Parameter" + Environment.NewLine);
					return;
				}
				if(!Directory.Exists(args[0]))
				{
					Console.WriteLine("cd: Invaild Directory" + Environment.NewLine);
					return;
				}
				Console.WriteLine(Environment.NewLine);
				currentFolder = args[0];
				Environment.CurrentDirectory = args[0];
				break;
			case "echo":
				if(args.Length != 1)
				{					
					Console.WriteLine("echo: Invaild Parameter" + Environment.NewLine);
					return;
				}
				Console.WriteLine(args[0] + Environment.NewLine);
				break;
			case "help":
				Console.WriteLine("Eagle Dash, running on " + Environment.Version.ToString() + " runtime" + Environment.NewLine + "version 0.1.1" + Environment.NewLine);
				Console.WriteLine(File.ReadAllText("dashhlp.txt") + Environment.NewLine);
				break;
			default:
				StartNewProg(name, args);
				Console.WriteLine();
			break;
		}
	}
}