using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Eagle.Utils
{
	public static class EagleConfiguration
	{
		public static string ReadEagleCfg(string filename, int line)
		{
			string[] vaildlne = ReadinVaildCfg(filename);
			return vaildlne[line];
		}
		
		public static void WriteEagleCfg(string filename, int lineWithRem, string val)
		{
			string[] lne = File.ReadAllLines(filename);
			lne[lineWithRem] = val;
			File.WriteAllLines(filename, lne);
		}
		
		public static string[] ReadinVaildCfg(string filename)
		{
			string[] lines = File.ReadAllLines(filename);
			List<string> applied = new List<string>();
			foreach(string line in lines)
			{
				if(!line.StartsWith("//"))
				{
					applied.Add(line);
				}
			}
			return applied.ToArray();
		}
	}
}