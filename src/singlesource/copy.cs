using System;
using System.IO;

class EagleCopy
{
	static void Main(string[] args)
	{
		if(args.Length != 2)
		{
			Console.WriteLine("copy: invaild arguments");
			return;
		}
		
		if(!File.Exists(args[0]))
		{
			Console.WriteLine("copy: source file not found");
			return;
		}
		
		try
		{
			File.Copy(args[0], args[1]);
		}
		catch(Exception ex)
		{
			Console.WriteLine("copy: exception thrown: " + Environment.NewLine + ex.ToString());
			return;
		}
	}
}